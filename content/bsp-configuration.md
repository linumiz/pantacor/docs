# BSP Configuration

BSP configuration at revision time.

This reference is for advanced users. Before manually editing these values in your device, check if what you intend to do can be achieved with the help of [pvr](pvr.md).

## run.json

### addons

Allows to add binary files to Pantavisor without any modification in its code. It's important to remark that the binary file has to have cpio.xz4 compression. That is, cpio.xz with 4 Byte alignment.

The following json object is used to import an addon:

```
{
	"addons": []
}
```

The table below describes the properties of the json object above:

| Property | Descripton                                              |
| -------- |:------------------------------------------------------- |
| addons   | list of addons with cpio.xz4 format                     |

For example, to get the gdbserver binary in Pantavisor rootfs:

```
{
	"addons": [
		"addon-gdbserver.cpio.xz4"
	]
}
```

You can take a look at how we do the cpio.xz4 compression [here](https://gitlab.com/pantacor/pantavisor-addons/gdbserver). For the rest of gdbserver installation and use, go [here](debug-pantavisor.md).
