# Pantacor Hub Client

## State Machine

* init: means we are in [remote mode](operation-modes.md) and the client has been initialized.
* register: device is trying to register as a non-claimed device in the cloud. This credentials will be stored after reboot.
* claim: device is registered and is waiting to be claimed.
* sync: device has been claimed and will try to upload the objects and state json to the cloud.
* idle: device has checked all objects and state json is in the cloud. This is where the device will spend most of its time: uploading devmeta, downloading usrmeta and waiting for updates.
* update: a new update has been received and the device will now download the objects and json before processing it.
