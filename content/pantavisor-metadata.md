# Pantavisor Metadata

Pantavisor metadata at run time. It allows data transfer between Pantahub and your devices.

There are two kinds of metadata:
* device metadata: generated from the device itself and sent to the cloud
* user metadata: generated from the cloud and sent to the device

## Device metadata

Device metadata can be created either by Pantavisor or any of the [running containers](pantavisor-commands.md).

This is the device metadata created by Pantavisor that will give you useful information about your device:

| Key | Value | Description |
| --- | ----- | ----------- |
| interfaces | json | network interfaces of the device |
| pantahub.claimed | 0 or 1 | 1 if claimed in Pantahub |
| pantahub.online | 0 or 1 | 1 of connection to Pantahub was established |
| pantahub.state | init, register, sync, idle or update | [see pantahub states](pantahub-client.md) |
| pantavisor.arch | string | CPU architecture |
| pantavisor.dtmodel | string | platform name |
| pantavisor.mode | local or remote | [see operation modes](operation-modes.md) |
| pantavisor.revision | string | [revision number](make-a-new-revision.md) |
| pantavisor.version | string | pantavisor build |

# User metadata

First, check out our user metadata [how to](set-device-metadata.md) guide if you don't know how to set it.

This is the user metadata that can be set by the user which is parsed and have some actions on Pantavisor:

| Key | Value | Description |
| --- | ----- | ----------- |
| pvr-sdk.authorized_keys | SSH pub key | set [public key](inspect-device.md) to get SSH access |
| pvr-auto-follow.url | URL | device will automatically pull every change in the device associated to that [clone URL](clone-your-system.md) |
| pantahub.log.push | 0 or 1 | disable/enable log pusshing to Pantahub. Enabled by default. Overrides [log.push](pantavisor-configuration.md#pantahub.config) |
| <config-key> | <config-value> | override some of the [configuration](pantavisor-configuration.md#during-runtime) values |
