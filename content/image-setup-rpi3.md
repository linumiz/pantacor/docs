# Setting up the image

There are two options for installing the pre-compiled initial image: 

* [From Pantacor Hub](#pantacor)
* [Outside of Pantacor Hub](#self-install)

## <a name="pantacor"> Download the pre-compiled initial image from Pantacor Hub </a>

The easiest way to try out the full Pantacor Hub experience is to install an initial, personalized Pantavisor-enabled image from Pantacor Hub. 

This contains a set of Linux based containers that provide basic network connectivity and discovery services and will automatically link the Raspberry Pi device to your Pantahub account for remote management.

Visit [your personalized pantavisor image page](https://hub.pantacor.com/download-image) and select your Raspberry Pi device.
We recommend you to select the "stable" release channel.

![](images/personalized-image-download.png)

## <a name="self-install"> Downloading the image outside of Pantacor Hub </a>

Find your raspberry Pi version in the target list and choose the stable channel: 

* [Download image](initial-images.md)

## Flash initial image

Install the image using your preferred tool. Specific instructions for your operating system can be found on the Raspberry Pi installation instructions for [Linux](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md), for [Mac OS](https://www.raspberrypi.org/documentation/installation/installing-images/mac.md) as well as for [Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md).

### Raspberry Pi Imager
For most users the [Raspberry Pi Imager](https://www.raspberrypi.org/software/) can flash our Pantavisor Image onto a micro SD card. Alternatively, check out the command line instructions to flash your device without additional software requirements.

1. Download the [Raspberry Pi Imager](https://www.raspberrypi.org/software/).
2. Select the initial image downloaded previously.
3. Select your micro SD card and click write.

![](images/raspberry-pi-imager.gif)

### Linux commands

For quick Linux instructions you can use the ```dd``` tool following these steps (remember to substitute `/dev/sdX` for the device node corresponding to your SD card, or else you will overwrite the wrong device!):

```
umount /dev/sdX*
gunzip -c  arm-rpi3.img.gz | sudo dd of=/dev/sdX bs=32M
sync
```

### Mac OS commands

For the Mac Os, the first step is to manually extract the `rpi3_initial_stable.img.xz` file. Then, find the SD card device name by opening a terminal and running the following command:

```
df -h
```

![](images/df-h.png)

You can see on the bottom a 15 GB disk /dev/disk3 is mounted on /Volimes/BOOT.

 Unmount it and flash it with the ```dd``` tool (remember to substitute `/Volumes/BOOT` and `/dev/disk3` for the device node corresponding to your SD card):

```
diskutil umount /Volumes/BOOT
```

![](images/unmount-sd-card.png)

```
sudo dd if=arm-rpi3.img of=/dev/disk3 bs=32m
sync
```

![](images/burn-image-to-sd-card.png)

Verify the image contents by opening the SD card partition named "boot":

![](images/verify-sd-card.png)

## Additional Notes: What does the image contain?

All Pantavisor devices must have a BSP (Linux Kernel, Pantavisor binary and Linux drivers) and then one to many containers.

In the case of the RPi3 and RPi4 base images, Pantavisor runs the following three Linux containers:

 * **awconnect**: automatically brings up basic cabled networking. If this fails, it creates a hotspot with SSID "Wifi Connect" that allows you to manually configure your device for WiFi connection.
 * **pv-avahi**: uses DNS multicast for device discoverability.
 * **pvr-sdk**: contains the Pantabox utilities to help with your development and debugging.

The example image provided is running a few more things next to Pantavisor. For example, an ssh server is available for inspecting each one of the containers as well as the root mount namespace. To get more familiar with the system internals, connect tty console support for boot up debugging.

The images in this get started guide are from our latest [stable version](initial-images.md#stable-initial-images). 

Go [here](choose-reference-device.md) to see how to build your own images.
