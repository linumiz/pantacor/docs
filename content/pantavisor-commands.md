# Pantavisor Control Socket

The pv-ctrl socket enables communication between the containers and Pantavisor during runtime.

The following subsections describe the behaviour of the HTTP API for the different endpoints, which you can test either using any HTTP client or our [pvcontrol tool](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/blob/master/files/usr/bin/pvcontrol) from the default [pvr-sdk container](https://gitlab.com/pantacor/pv-platforms/pvr-sdk).

**NOTE:** The examples provided use cURL, but any HTTP client inside of your container should work.

## /commands

These commands can perform changes in Pantavisor state machine, so the result will not be inmediate to the request.

An example of a command that tells Pantavisor to transition to revision 4:

```
$ curl -X POST --header "Content-Type: application/json" --data "{\"op\":\"LOCAL_RUN\",\"payload\":\"4\"}" --unix-socket /pantavisor/pv-ctrl http://localhost/commands
```

As you can see, the body of the request contains the command itself in JSON format.

These are the different commands that are supported. You can test them by substituting ```op``` and ```payload``` in the above command:

| op | payload | Description |
| -- | ------- | ----------- |
| LOCAL_RUN | [revision](make-a-new-revision.md) | transition to specified revision |
| POWEROFF_DEVICE | message | poweroff device with optional message |
| REBOOT_DEVICE | message | reboot device with optional message |
| MAKE_FACTORY | revision | make the revision the factory revision. If revision is not set, Pantavisor will use the current one. Device needs to be [not claimed](claim-device.md) |
| UPDATE_METADATA | {key, value} | upload the json as a new pair of device metadata to Pantahub |
| RUN_GC | N/A | run garbage collector |

## /objects

This endpoint can be used to list, send and receive objects to and from Pantavisor. Bear in mind that objects are the artifacts that form a Pantavisor revision.

An example of listing the objects that are stored in the device:

```
$ curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/objects"
```

Here, an example for getting one of those objects:

```
curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/objects/033e779113f2499a2bfb55c0c374803fba9c820361d71bbda616643007cacd5a"
```

You can even put new objects in Pantavisor. Notice that the sha256sum of object has to match the specified sha in the URI:

```
curl -X PUT --upload-file object --unix-socket /pantavisor/pv-ctrl "http://localhost/objects/033e779113f2499a2bfb55c0c374803fba9c820361d71bbda616643007cacd5a"
```

## /steps

This endpoint can be used to list, send and receive step jsons, as well as get the update progress and set the commit message for any of them.

Let us go with the examples, first, to list all steps installed in the device:

```
curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/steps"
```

To get an existing step json:

```
curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/steps/033e779113f2499a2bfb55c0c374803fba9c820361d71bbda616643007cacd5a"
```

To send a new json, the format of the new revision in the URI has to contain the "locals/" prefix. The name after the prefix must be under 64 characters and must not contain any other "/" character. These revisions that are installed using the socket ([locals](operation-modes.md#local)) are treated in a different way than the ones installed from Pantahub ([remotes](operation-modes.md#remote)), as you will have to manually request the transition to locals using the [run command](#/commands). Most importantly, locals will not attempt any communication with Pantahub during runtime.

```
curl -X PUT --upload-file json --unix-socket /pantavisor/pv-ctrl "http://localhost/steps/locals/example"
```

To get the update progress (DONE, TESTING, INPROGRESS...) and some related information of a revision:

```
curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/steps/033e779113f2499a2bfb55c0c374803fba9c820361d71bbda616643007cacd5a/progress"
```

Finally, you can set a commit message that will be stored along a revision and showed when listing revisions so the user can idendifcate each one of them:

```
curl -X PUT --data "message" --unix-socket /pantavisor/pv-ctrl "http://localhost/steps/033e779113f2499a2bfb55c0c374803fba9c820361d71bbda616643007cacd5a/commitmsg"
```

## /user-meta

The user-meta endpoint offers the ability to list, save and delete [user metadata](pantavisor-metadata.md#user-metadata).

To list all user meta in json format:

```
curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/user-meta"
```

An example of creating or updating a new user metadata pair in device. This pair will only persist in [local mode](operation-modes.md#local), as the cloud will overwrite whatever is in the device during [remote mode](operation-modes.md#remote):

```
curl -X PUT --data value --unix-socket /pantavisor/pv-ctrl "http://localhost/user-meta/key"
```

To delete one pair, we would do this, having in mind the same behaviour of operation modes as with putting metadata pairs:

```
curl -X DELETE --unix-socket /pantavisor/pv-ctrl "http://localhost/user-meta/key"
```

## /device-meta

The device-meta endpoint offers the ability to list [device metadata](pantavisor-metadata.md#device-metadata).

To list all device meta in json format:

```
curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/device-meta"
```

## /buildinfo

For debugging porpuses, it is possible to get the repo manifest that was used to build this pantavisor binary.

With cURL, this would look like:

```
curl -X GET --unix-socket /pantavisor/pv-ctrl "http://localhost/buildinfo"
```
