# Garbage Collector

The Pantavisor garbage collector is in charge of cleaning up disk usage by automatically removing unused Pantavisor artifacts.

It works removing logs, revision specifications and persistent stored disks belonging to old revision. After all this, all orphan objects that were linked to removed revisions will be deleted.

The garbage collector will not affect any of the files related to the running revision, the revision that is being updated or the latest [DONE](deploy-a-new-revision.md) revision. These revisions not affected by the garbage colletor can be expanded to the factory revision using the [storage.gc.keep_factory configuration parameter](pantavisor-configuration.md#pantavisor.config).

Currently, it can be triggered by three different events:
* by a remote update that requires more disk space than available
* if a threshold of disk usage is surpassed
* with a command

## Remote update

If a remote update requires more disk space than available, the garbage collector will be activated. This can be adjusted with the configuration [storage.gc.reserved parameter](pantavisor-configuration.md#pantavisor.config).

**NOTE:** this type of trigger will not work with local updates. For those, you will need to use one of the following two.

## Treshold

Disk usage will be checked periodically and garbage collector will be activated if that theshold is reached. By default it is disabled. This can be changed with the [storage.gc.threshold parameter](pantavisor-configuration.md#pantavisor.config).

If an object is put using the [objects endpoint](pantavisor-commands.md#objects), the threshold will be disabled temporarily, which is done to avoid removing objects that could be linked to the upcoming new revision. This parameter can be changed from the [configuration](pantavisor-configuration.md#pantavisor.config).

## On Demand

Finally, our garbage collector can be triggered on demand issuing a [command](pantavisor-commands.md#commands) through Pantavisor control socket.
