# Operation Modes

[Pantavisor control socket](pantavisor-commands.md) allows for fully interaction with the device without cloud. As a consequence, Pantavisor can operate in two different modes.

## Remote

This is the traditional way of doing things for Pantavisor. To enter this mode, you will have to [claim your device](claim-device.md). From that moment on, your device will try to constantly keep connection with Pantahub, receiving [user metadata](pantavisor-metadata.md#user-metadata) that is [set](set-device-metadata.md) on the cloud, sending [device metadata](pantavisor-metadata.md#device-metadata) from the device or any of the containers, and of course, waiting for [new updates](deploy-a-new-revision.md) posted in Pantahub.

## Local

Any device that is not claimed is waiting to be registered and [claimed](claim-device.md) in Pantahub, unless you set [local mode](pantavisor-configuration.md#pantavisor.config) permanently before build time. Even if you do not configure it that way, communication can be cut down in remote mode after an update from [pv-ctrl socket API](pantavisor-commands.md). After the new locals revision is put and run, the device will no longer try to communicate with Pantahub. Local mode can be reverted by running again a run command back to a remote revision.

This mode will leave all device management in the hands of the pv-ctrl client. With the help of the HTTP API you will be able to execute orders into the Pantavisor state machine, clone and install revisions and manage metadata.
