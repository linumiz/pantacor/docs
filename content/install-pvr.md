---
title: How to install the PVR cli
description: Interact with your Pantavisor-enabled devices remotely through Pantacor Hub with the PVR cli. 
---

## **#1.** **Install pvr**

 ```pvr``` enables you to interact with your Pantavisor-enabled devices remotely through Pantacor Hub. The CLI can ```clone``` your device and ```post``` different demos to the Hub for testing and seamless onboarding, among other operations.

## **#2.** **Get PVR stable**

You can download it from the following locations:

 * [PVR for Linux/AMD64](https://gitlab.com/pantacor/pvr/-/jobs/595489669/artifacts/raw/pvr-012.linux.amd64.tar.gz#latest)
 * [PVR for Linux/ARM32V6](https://gitlab.com/pantacor/pvr/-/jobs/595489669/artifacts/raw/pvr-012.linux.armv6.tar.gz#latest)
 * [PVR for Darwin/AMD64](https://gitlab.com/pantacor/pvr/-/jobs/595489669/artifacts/raw/pvr-012.darwin.amd64.tar.gz#latest)
 * [PVR for Windows/x32](https://gitlab.com/pantacor/pvr/-/jobs/595489669/artifacts/raw/pvr-012.windows.x32.zip#latest)
 * [PVR for Windows/x64](https://gitlab.com/pantacor/pvr/-/jobs/595489669/artifacts/raw/pvr-012.windows.x64.zip#latest)


## **#3.** **Install the pvr binary**

### Linux

To install the Linux version you need to extract and place the binary in your ```$PATH```:

```
tar xvzf pvr-012.linux.amd64.tar.gz 
mkdir -p ~/bin
cp pvr ~/bin/pvr
chmod +x ~/bin/pvr
export PATH=$PATH:~/bin
```

### Windows

To install the Windows version all you need is unzip the binary and place it in a directory to which your user has access and can run executables from. ```C:\Users\YOURUSER``` is usually a good location.

## **#4.** **Test pvr**

Once you have it installed just calling the ```pvr``` command from your shell should show you the help menu, where you can get familiarized with the different features that it provides.

You can also take a look at the [maintain your system how-to guide](clone-your-system.md) for a view on how you can interact with your devices using ```pvr```.

## **#5.** **Get last PVR version**

If you prefer to use the last ```pvr``` version instead of using the stable one, you can do the following:

```
pvr global-config DistributionTag=develop
pvr self-upgrade
```

The stored binary will automatically upgrade to the last version.

## **#6.** **Next steps**

Once downloaded and installed continue with [signing up to Pantacor Hub](https://hub.pantacor.com) or if you want to use Pantabox, then continue on to [Getting Started on Raspberry Pi](get-started.md) or [Getting Started on Other Devices](choose-image.md)