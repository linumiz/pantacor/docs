# Container Configuration

Container configuration at revision time.

This reference is for advanced users. Before manually editing these values in your device, check if what you intend to do can be achieved with the help of [pvr](pvr.md).

## run.json

### Logs

Allows configuring the pushing of logs to Pantahub from each container. There are three types of pushable to Pantahub logs: app, LXC and console logs.

#### App logs

For pushing logs from within container to pantahub, the following json object is used under the logs object:

```
{
	"file":,
	"maxsize":,
	"truncate":,
	"name":;
}
```

The table below describes the properties of the json object above,

| Property | Descripton                                              |
| -------- |:------------------------------------------------------- |
| file     | Absolute location of the log file within the container. |
| maxsize  | max file size in bytes before truncating.               |
| truncate | true \| false                                           |
| name     | logger daemon name                                      |

One dedicated logger is created by Pantavisor for every such object found. For example:

```
"logs":[
		{"file":"/var/log/syslog","maxsize":102485760,"truncate":true,"name":"alpine-logger"},
		{"file":"/tmp/app.log","maxsize":102485760,"truncate":true,"name":"app-logger"}
	]
```

####  LXC log

Enabled by default. Log truncation maxsize is 2MiB. The following json object is used under the logs object:

```
{
	"lxc":,
	"maxsize":,
	"truncate":,
	"name":
}
```

| Property | Descripton                                                       |
| -------- |:---------------------------------------------------------------- |
| lxc      | use keyword "enable" to push lxc startup logs to Pantahub. |
| maxsize  | max file size in bytes before truncating.                        |
| truncate | true \| false                                                    |
| name     | logger daemon name                                               |

May be added to run.json logs to change the default settings. For example, for disabling truncation:

```
"logs":[
		{"lxc":"enable","maxsize":204971520,"truncate":false,"name":"alpine-lxc"},
    ]
```

#### Console log

Enabled by default. Log truncation maxsize is 2MiB. The following json object is used under the logs object:

```
{
	"console":,
	"maxsize":,
	"truncate":,
	"name":
}
```

| Property | Descripton                                                       |
| -------- |:---------------------------------------------------------------- |
| console  | use keyword "enable" to push lxc startup logs to Pantahub.|
| maxsize  | max file size in bytes before truncating.                        |
| truncate | true \| false                                                    |
| name     | logger daemon name                                               |

May be added to run.json logs to change the default settings. For example for changing truncation maxsize to 512KiB:

```
"logs":[
		{"console":"enable","maxsize":51242880,"truncate":true,"name":"alpine-console"}
    ]
```

### Storage

Allows to change the persitence of a container rootfs. There are three types of persitence:

* permanent: changes are stored locally permanently. Not changing if a new revision is pushed or redeployed.
* revision: changes are pegged locally to the revision. A redeploy of a previous revision will recover the changes of that revision and delete the changes of the current one. A push of a new revision will not delete them.
* boot: changes are volatile like in any tmpfs. Any push or redeploy will delete the changes in the rootfs.

The following json object is used under the storage object to change persistence:

```
{
	"lxc-overlay":{
        "persistence":
    }
}
```

| Property    | Descripton                                                       |
| ----------- |:---------------------------------------------------------------- |
| persistence | "permanent", "revision" and "boot"                               |

For example, if you want to change the container rootfs persistence to boot:

```
"storage":{
    "lxc-overlay":{
        "persistence":"boot"
    }
}
```

On the other hand, to change the persistence of a specific path:

```
"storage": {
    "etc/example/": {
        "persistence": "permanent"
    }
}
```

### Runlevel

Runlevel defines the hierarchy of containers. It can be set for each container with any of the three valid runlevels: root, platform or app. Pantavisor will use these values to decide whether a board reboot is needed during an update and which order to follow when starting or stopping the containers. 

* root container: it is recommended to set as root the container or containers in charge of setting up network connectivity for the board.
* platform container: value by default when runlevel is not explicitly configured. Can be set to get the same behaviour as for root containers but still honor the container order.
* app contaienr: rest of the containres will fall into this category. It is recommended for the containers that contain the application level functionality.

When a new revision is posted to your device, updates will behave differently depending on what changes and how the containers are configured, with two possibilities:

* Reboot update: this kind of update is performed if the revision contains any change inside of the bsp folder, inside any of the folders of the root runlevel containers, or inside any of the folders of the platform runlevel containers (or non-explicitly configured). The action order for this kind of updates is: stop app containers, stop platform containers, stop root containers, reboot, boot kernel and Pantavisor, start root containers, start platform containers and start app containers.
* Non-reboot update: this update is triggered if the new revision contains any change inside of the folder of any of the app runlevel containers. In this case, we avoid some of the actions performed in the reboot update, keeping the board on and connected to the Internet. The order of actions would be: stop app containers and start app containers.

**IMPORTANT:** In order to avoid loosing connectivity in non-reboot updates, it is important to make sure that the container configured as root is in fact the one providing the connectivity.

The following json object can be used to set the runlevel of a container:

```
{
    "runlevel":"root"
}
```

| Property | Descripton      |
| -------- |:--------------- |
| runlevel | "root", "platform" or "app" |

### Roles

Roles is a set of functions that can be granted to each container. Each container can have from zero to any number of roles.

Roles available:
* mgmt: Set by default. Pantavisor will mount the full /pv directory to containers with this role. This includes full logs of all containers from all revisions, pv-ctrl-log and pv-ctrl sockets, full user-meta, challenge and device-id files. If mgmt is not configured, Pantavisor will only mount pv-ctrl-log socket, logs for the container in the current revision and user-meta assigned to that container (all user-meta that starts with "plat-name.").

The following json array can be used to set the roles of a container:

```
{
    "roles":["mgmt"]
}
```

**IMPORTANT:** By default, mgmt role is set for every container. To remove it, it is necessary to define an empty roles array in its run.json:

```
{
    "roles":[]
}
```

