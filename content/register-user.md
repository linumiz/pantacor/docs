# Sign up to Pantacor Hub

The first thing you need to do to interact with Pantacor Hub is to register a user account. A user account gives you access to the full API, including the object store, and also grants you access to the dashboard on [https://hub.pantacor.com](https://hub.pantacor.com).

You can sign up with the [pvr](install-pvr.md) command, in Pantabox with the [pantabox-claim](https://docs.pantahub.com/pvr-sdk/reference/pantabox-claim/) command or in the [Pantacor Hub web interface](https://hub.pantacor.com).

!!! Note
    After registering your account, make sure to follow the instruction in the verification email. Your account is not ready for use until it is verified.

## Registering on the web 

Visit the Pantacor Hub starting page at <https://hub.pantacor.com> and follow the sign-up process. You have the option of signing up with your email address or with your Google, GitHub or GitLab account.

![](images/ph-welcome.png)

## Registering with [pvr](install-pvr.md) command
You can also use the following [pvr](install-pvr.md) command to register a user:

```pvr register -u youruser -p yourpassword -e your@email.tld```

This will generate a json response with the server-generated part of the credentials:

```
2017/06/19 11:08:43 Registration Response: {
  "id": "5947949b85188a000c143c2e",
  "type": "USER",
  "email": "your@email.tld",
  "nick": "youruser",
  "prn": "prn:::accounts:/5947949b85188a000c143c2e",
  "password": "yourpassword",
  "time-created": "2017-06-19T09:08:43.767224118Z",
  "time-modified": "2017-06-19T09:08:43.767224118Z"
}
```
## Registering with Pantabox

To register through Pantabox, you must have already downloaded an initial precompiled image, flashed your device and logged in to the device.   Go to one of the Getting Started Guides, either [Raspberry Pi](get-started.md) or [Other Devices](choose-image.md) and then follow the instructions on the screen to log in.  

After you've logged in to Pantabox, claim the device with [`pantabox-claim`](https://docs.pantahub.com/pvr-sdk/reference/pantabox-claim/)command.