---
title: Getting Started with Pantavisor and Pantacor Hub
description: To begin with Pantavisor install either the PVR cli or use Pantabox, a set of utilities installed with the pre-compiled initial image. 
---

### PVR CLI vs. Pantabox Utilities
To begin using Pantavisor and Pantacor Hub, you can either install the `pvr` command line interface (cli) or you can take advantage of the Pantabox utilities that come with an initial prepared image. 

If you're not using the cli, then jump ahead to a tutorial that matches your device:

* [Install PVR CLI](install-pvr.md)

If you don't want to use PVR command line utility and prefer the option of working locally on the device, then download an initial pre-compiled image for your device type that includes our convenient [Pantabox utilities](initial-images.md). You will also be able to log into and sign up for Pantacor Hub from Pantabox, but this is not required. 

!!! Note
    There's no harm in having both the cli and Pantabox at your disposal. They both have the same functionality and you may find one more convenient over the other. 

* [Initial Image with Pantabox utilities](initial-images.md)

### Getting started with Raspberry Pi
After you've either installed the PVR CLI or you've opted to use the Pantabox utilities from the initial image for Raspberry Pi, then follow one of these beginner tutorials:

* [Getting started on Raspberry Pi](get-started.md)
* [Prepare your device and run an app from the Marketplace](https://www.pantavisor.io/guides/getting_started/)

### Getting started with other devices
If you are not using a Raspberry Pi, take a look at these guides:

* [Installing images for other targets](choose-image.md)
* [Getting started with LimeSDR on Raspberry Pi](limesdr-initial-devices.md)

### After your device is set up
Once your device is setup, take advantage of Pantacor Hub and Pantavisor to develop and deploy applications and system components to your device. These walk-throughs describe how to do that:

* [How to make changes to your device](clone-your-system.md)
* [How to build your own apps](create-apps.md)
* [How to build Pantavisor from source code](choose-reference-device.md)

### Technical deep dives and API integration
For more in-depth documentation on how to build Pantavisor and what it's architecture looks like, here are some reference guides and other tutorials:

* [Technical deepdives](pantavisor-architecture.md)
* [Pantacor hub API reference](https://docs.pantahub.com/pantahub-base/auth/#authenticate)
* [Pantavisor build options reference](build-options.md)
* [PVR command reference](pvr.md)
* [Other turorials](mozilla-iot-gateway-as-a-pantavisor-app.md)