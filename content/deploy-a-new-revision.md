# Deploy a new System Revision

Once you have done some changes like described in previous section, you can deploy the currently staged pvr state to any device for which you have owner permissions.

For that use ```pvr post``` command.

```pvr post``` has an optional argument where you can specify a pvr clone URL for the device you want the currently staged system state to be posted to.

Once post has been submitted to a device, the device will eventually wake up and try to consume the new state.

## Notes: How does the update process work?

First, disk space will be checked for the new revision. If space is not enough, Pantavisor will try to make some room for the update with our garbage collector, which will remove artifacts and logs from older revisions. You can see [here](pantavisor-configuration.md#pantavisor.config) how our garbage collector can be configured.

After that, the download and installation of the new update will take place. Your device will report to Pantahub with the ```DOWNLOADING``` and ```INPROGRESS``` states.

Now, depending on the [runlevel](app-configuration.md#runlevel) of the update, the device might reboot or not. After that, you will be able to see your device has changed to ```TESTING````, a [configurable](pantavisor-configuration.md#pantahub.config) period of time in which Pantavisor will check if connectivity of the device is stable. 

If the device got rebooted, the final state of the update process will be ```DONE```. This means the device will take this revision as a fully-bootable stable check-point to rollback if anything goes wrong in future updates. On the contrary, if the update did not need a reboot, the final state will be ```UPDATED```, which means the update finished correctly, but the revisions is not set as a rollback check-point. Manually reboot your device to progress the state from ```UPDATED``` to ```DONE``` if you want to set the check-point to that revision.
