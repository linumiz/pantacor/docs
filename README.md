# README

## How to run it with livereload

```bash
git clone --recurse-submodules git@gitlab.com:pantacor/docs.git
cd docs
docker-compose up docs
```

Go to http://localhost:8080/ to your livereload server

## How to locally test changes in this repo

To check how your changes in docs look like, you can use docker:

```
docker build -f Dockerfile -t docs .
docker run -p 8080:80 docs
```

A nginx instance is now running locally. In your web browser, go to http://localhost:8080/ to access the built docs.
